import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeVaraPage } from './home-vara.page';

const routes: Routes = [
  {
    path: '',
    component: HomeVaraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeVaraPageRoutingModule {}
