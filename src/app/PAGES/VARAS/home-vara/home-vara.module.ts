import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeVaraPageRoutingModule } from './home-vara-routing.module';

import { HomeVaraPage } from './home-vara.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeVaraPageRoutingModule
  ],
  declarations: [HomeVaraPage]
})
export class HomeVaraPageModule {}
