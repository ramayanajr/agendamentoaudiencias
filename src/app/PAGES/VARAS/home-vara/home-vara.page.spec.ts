import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeVaraPage } from './home-vara.page';

describe('HomeVaraPage', () => {
  let component: HomeVaraPage;
  let fixture: ComponentFixture<HomeVaraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeVaraPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeVaraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
