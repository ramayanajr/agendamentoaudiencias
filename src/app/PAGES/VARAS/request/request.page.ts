import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-request',
  templateUrl: './request.page.html',
  styleUrls: ['./request.page.scss'],
})
export class RequestPage implements OnInit {

  day_after_tomorrow: string;
  two_years_later: string;
  request: any = {};

  constructor(
    private toastCtrl: ToastController,
    private router: Router
  ) {}

  ngOnInit() {

    // start date - at the earliest the day after tomorrow
    let today = new Date();
    let day_after_tomorrow = new Date(today.getTime() + 1000 * 60 * 60 * 24 * 2);
    this.day_after_tomorrow = day_after_tomorrow.toISOString().slice(0, 10);

    // end date - at the latest in two years
    let two_years_later
      = new Date(day_after_tomorrow.getTime()
        + 1000 * 60 * 60 * 24 * 365 * 2);
    this.two_years_later = two_years_later.toISOString()
      .slice(0, 10);
  }

  // user clicked 'Send request'
  send() {

    this.confirm();

    console.log('Requested audience  for',
      this.request.Date,
      this.request.Time);

  

    console.log('To',
      this.request.FirstName,
      this.request.LastName,
      this.request.Cpf);
  }

  // user clicked 'Cancel'
  cancel() {
    this.router.navigate(['/home-vara']);
}

  // Confirmation after sending the request.
  async confirm() {
    const toast = await this.toastCtrl.create({
      message: 'Solicitação concluída, aguarde a confirmação da SEAP',
      duration: 3500
    });
    toast.present();
  }

}
