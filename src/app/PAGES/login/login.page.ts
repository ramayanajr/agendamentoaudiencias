import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  usuario: string = "";
  senha: string = "";
  perfil: string = "SEAP"; //A SER BUSCADO DO BANCO DE DADOS
  sessionStorage: any = {};

  constructor(private storage: Storage, private router: Router, public toast: ToastController) { }

  ngOnInit() {

  }

  async login() {
    if (this.usuario == "") {
      const toast = await this.toast.create({
        message: 'Preencha o Usuário',
        duration: 2000,
        color: 'warning'
      });
      toast.present();
      return;
    } else if (this.senha == "") {
      const toast = await this.toast.create({
        message: 'Preencha a Senha',
        duration: 2000,
        color: 'warning'
      });
      toast.present();
      return;
    } else {



      /*  let dados = {
            requisicao : 'login',
            usuario : this.usuario, 
            senha : this.senha
            
            }; */

      //this.provider.dadosApi(dados, 'api.php').subscribe(async data => {
      // var alert = data['msg'];
      // if(data['success']) {
      let logado: any = { usuario: this.usuario, senha: this.senha, perfil: this.perfil };
      this.storage.set('session_storage', logado);
      if (logado['perfil'] == 'VARA') {
        this.router.navigate(['/home-vara']);
      } else if (logado['perfil'] == 'SEAP') {
        this.router.navigate(['/home']);
      }



      const toast = await this.toast.create({
        message: 'Logado com Sucesso!!',
        duration: 1000,
        color: 'success'
      });
      toast.present();
      /*  this.usuario = "";
        this.senha = ""; */
     // console.log(logado);
     // console.log(this.sessionStorage);
    }/* else{
          const toast = await this.toast.create({
            message: alert,
            duration: 2000,
            color: 'danger'
          });
          toast.present(); */

  }

}
