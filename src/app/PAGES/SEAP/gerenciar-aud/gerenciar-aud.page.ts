import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-gerenciar-aud',
  templateUrl: './gerenciar-aud.page.html',
  styleUrls: ['./gerenciar-aud.page.scss'],
})
export class GerenciarAudPage implements OnInit {

  constructor(private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController) { }

  ngOnInit() {
  }

}
