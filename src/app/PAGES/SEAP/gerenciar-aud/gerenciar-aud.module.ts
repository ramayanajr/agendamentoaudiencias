import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GerenciarAudPageRoutingModule } from './gerenciar-aud-routing.module';

import { GerenciarAudPage } from './gerenciar-aud.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GerenciarAudPageRoutingModule
  ],
  declarations: [GerenciarAudPage]
})
export class GerenciarAudPageModule {}
