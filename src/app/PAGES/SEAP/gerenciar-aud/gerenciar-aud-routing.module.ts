import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GerenciarAudPage } from './gerenciar-aud.page';

const routes: Routes = [
  {
    path: '',
    component: GerenciarAudPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GerenciarAudPageRoutingModule {}
