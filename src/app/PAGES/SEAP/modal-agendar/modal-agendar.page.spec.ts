import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalAgendarPage } from './modal-agendar.page';

describe('ModalAgendarPage', () => {
  let component: ModalAgendarPage;
  let fixture: ComponentFixture<ModalAgendarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAgendarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalAgendarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
