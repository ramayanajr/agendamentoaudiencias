import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AprovarAudPage } from './aprovar-aud.page';

describe('AprovarAudPage', () => {
  let component: AprovarAudPage;
  let fixture: ComponentFixture<AprovarAudPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprovarAudPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AprovarAudPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
