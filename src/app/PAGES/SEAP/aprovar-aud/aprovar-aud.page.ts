import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModalAgendarPage } from '../modal-agendar/modal-agendar.page';

@Component({
  selector: 'app-aprovar-aud',
  templateUrl: './aprovar-aud.page.html',
  styleUrls: ['./aprovar-aud.page.scss'],
})
export class AprovarAudPage implements OnInit {

  audiencias: any = [{ Icon: "people-circle-outline",
Name: "Rayana Corrêa",
Count: 2,
ID: 1}, { Icon: "people-circle-outline",
Name: "Brendon vulgo Bruno",
Count: 3,
ID: 2}]

constructor(private activatedRoute: ActivatedRoute,
                 private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
    component: ModalAgendarPage,
    componentProps: this.audiencias
    });
    modal.present();
    console.log("oi");
    }

}
