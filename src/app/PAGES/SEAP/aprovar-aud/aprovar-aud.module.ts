import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AprovarAudPageRoutingModule } from './aprovar-aud-routing.module';

import { AprovarAudPage } from './aprovar-aud.page';
import { ModalAgendarPage } from '../modal-agendar/modal-agendar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AprovarAudPageRoutingModule
  ],
  declarations: [AprovarAudPage, ModalAgendarPage],
  entryComponents: [ModalAgendarPage]
})
export class AprovarAudPageModule {}
