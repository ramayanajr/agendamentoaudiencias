import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AprovarAudPage } from './aprovar-aud.page';

const routes: Routes = [
  {
    path: '',
    component: AprovarAudPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AprovarAudPageRoutingModule {}
