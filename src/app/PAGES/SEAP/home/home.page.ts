import { Component, ViewChild } from '@angular/core';
import { CalendarComponent } from 'ionic2-calendar';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  eventSource = [];
  viewTitle: string;

  calendar = {
    mode: 'month',
    currentDate: new Date()
  };

  @ViewChild(CalendarComponent) tjCal: CalendarComponent;

  constructor() {}

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  next() {
    this.tjCal.slideNext();
  }

  back() {
    this.tjCal.slidePrev();
  }

}
