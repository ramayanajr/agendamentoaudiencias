import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./PAGES/SEAP/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home-vara',
    loadChildren: () => import('./PAGES/VARAS/home-vara/home-vara.module').then( m => m.HomeVaraPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./PAGES/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'request',
    loadChildren: () => import('./PAGES/VARAS/request/request.module').then( m => m.RequestPageModule)
  },
  {
    path: 'favoritas',
    loadChildren: () => import('./PAGES/favoritas/favoritas.module').then( m => m.FavoritasPageModule)
  },
  {
    path: 'gerenciar-aud',
    loadChildren: () => import('./PAGES/SEAP/gerenciar-aud/gerenciar-aud.module').then( m => m.GerenciarAudPageModule)
  },  {
    path: 'aprovar-aud',
    loadChildren: () => import('./PAGES/SEAP/aprovar-aud/aprovar-aud.module').then( m => m.AprovarAudPageModule)
  },
  {
    path: 'modal-agendar',
    loadChildren: () => import('./PAGES/SEAP/modal-agendar/modal-agendar.module').then( m => m.ModalAgendarPageModule)
  }






];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
