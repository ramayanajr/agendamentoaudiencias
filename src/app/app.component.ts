import { Component } from '@angular/core';

import { Platform, PopoverController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { NavigationEnd, Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public appPages = [
  
    {
      title: 'Home', //tela calendário
      url: '/home-vara',
      icon: 'home'
    },
    {
      title: 'Solicitar Audiência',
      url: '/request',
      icon: 'desktop'
    },
    {
      title: 'Favoritas',
      url: '/favoritas',
      icon: 'star'
    }
  ];

  public appSeap = [
  
    {
      title: 'Home', //tela calendário
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Gerenciar Audiências',
      url: '/gerenciar-aud',
      icon: 'desktop'
    },
    {
      title: 'Favoritas',
      url: '/favoritas',
      icon: 'star'
    }
  ];

  settings: any = {};
  periodo: any = { lower: 0, upper: 800 };
  hits: number = 5;
  url: string;
  urlsplit: any = [];
  sessionStorage: any = {};
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
   // private btService: BobToursService,
    private popoverCtrl: PopoverController,
    private storage: Storage, 
    private router: Router
  ) {

    router.events.subscribe(event => {

      if (event instanceof NavigationEnd ) {
        this.url = event.url; 
        this.urlsplit = this.url.split("/");
        console.log(this.urlsplit);
        this.loadSession();
      }
    });
  
    this.initializeApp();
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    //  this.btService.initialize();
      this.loadSettings();
    });
  }

  // Load settings
  async loadSettings() {
  
  await this.storage.ready().then(async () => {
    await  this.storage.get('settings').then(settings => {
        if (settings == null) {
          this.settings.style = 'summer-style';
          console.log(this.settings);
        } else {
          this.settings = settings;
        }
      }); 
    });
  }

  async loadSession() {
    await this.storage.ready().then(async () => {
      await  this.storage.get('session_storage').then(sessionStorage => {
          if (sessionStorage == null) {
            this.router.navigate(['/login']);
            console.log(this.sessionStorage);
          } else {
            this.sessionStorage = sessionStorage;
            console.log(this.sessionStorage.perfil);
          }
        });
      });
    }

  // User has changed his/her settings.
  updateSettings() {
  
    this.storage.set('settings', this.settings);
   console.log(this.settings);
  
  }

  // User clicked on 'About this app'
  async about() {
  /*   const popover = await this.popoverCtrl.create({
      component: AboutComponent,
      translucent: true
    });
    await popover.present(); */
  } 
  // User has changed periodo range.
  filterByPeriodo() {
   // this.hits = this.btService.filterTours(this.periodo);
  }
}
